@chcp 65001
@rd /s /q downloader
@git clone git@gitlab.com:1script/downloader.git
@cd downloader
@git checkout master
@oscript.exe src/ОсновнойСкрипт.os %*
@if errorlevel 1 GOTO error
@cd ..
@rd /s /q downloader
@exit /b %ERRORLEVEL%

:error
@cd ..
@rd /s /q downloader
@EXIT 1