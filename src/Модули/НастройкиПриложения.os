#Использовать json

Перем Лог;

Перем КаталогПараметров Экспорт;
Перем Параметры Экспорт;

Процедура ПрочитатьПараметры() Экспорт
	
	Ожидаем.Что(АргументыКоманднойСтроки.Количество()).Больше(0);

	ФайлПараметров = АргументыКоманднойСтроки[0];
	
	Ожидаем.Что(ФайлПараметров).Заполнено();
	
	Файл = Новый Файл(ФайлПараметров);
	Если НЕ Файл.Существует() Тогда
		ВызватьИсключение СтрШаблон("Файл параметров не существует: %1", ФайлПараметров);
	КонецЕсли;
	КаталогПараметров = Файл.Путь;

	Лог.Отладка("Получаем параметры из файла: %1", ФайлПараметров);

	Текст = Служебный.ФайлВТекст(ФайлПараметров);
	
	Парсер = Новый ПарсерJSON();
	Параметры = Парсер.ПрочитатьJSON(Текст, , , Истина);
	
	// постдоработка
	Параметры.Вставить("АдресПроекта", "https://releases.1c.ru/project/" + Параметры.ИмяПроекта);	
	Параметры.Вставить("КаталогПараметров", Файл.Путь);

	ДопПараметры = Новый Соответствие();
	Если АргументыКоманднойСтроки.Количество() > 1 Тогда
		Для поз = 2 По АргументыКоманднойСтроки.Количество() Цикл
			ДопПараметры.Вставить(АргументыКоманднойСтроки[поз-1], Истина);
		КонецЦикла;
	КонецЕсли;
	Параметры.Вставить("ДопПараметры", ДопПараметры);
	
	Для каждого КЗ Из Параметры Цикл
		КоличествоПробелов = 30 - СтрДлина(КЗ.Ключ);
		Лог.Отладка("   - параметр %1 : %2 %3", КЗ.Ключ, Служебный.ПовторСтроки(" ", КоличествоПробелов), КЗ.Значение);
	КонецЦикла;

КонецПроцедуры

Лог = Логирование.ПолучитьЛог("oscript.lib.downloader_releases_1c");
