
Процедура СохранитьТекстВФайл(Текст, ИмяФайла) Экспорт
	
	ЗаписьТекста = Новый ЗаписьТекста(ИмяФайла);
	ЗаписьТекста.Записать(Текст);
	ЗаписьТекста.Закрыть();

КонецПроцедуры

Функция ВерсияЧислом(Знач Версия, Разделитель=".") Экспорт
	ЧастиВерсии = СтрРазделить(Версия, Разделитель, Ложь);
	Если ЧастиВерсии.Количество() <= 1 Тогда
		Возврат 0;
	КонецЕсли;

	ВерсияКакЧисло = 0;
	Разрядов = 4;
	Для инд = 1 По Разрядов Цикл
		Если ЧастиВерсии.Количество() >= Инд Тогда
			ВерсияКакЧисло = ВерсияКакЧисло + ЧастиВерсии[инд-1] * Pow(1000, Разрядов-инд);
		КонецЕсли;
	КонецЦикла;
	Возврат ВерсияКакЧисло;
КонецФункции

Функция ОбщийПутьФайлов(МассивФайлов) Экспорт

	ОбщийПуть = Лев(МассивФайлов[0].ПолноеИмя, СтрДлина(МассивФайлов[0].ПолноеИмя) - СтрДлина(МассивФайлов[0].Имя));
	Для й = 1 По МассивФайлов.ВГраница() Цикл
		Если МассивФайлов[й].Имя = МассивФайлов[й].ПолноеИмя Тогда
			Прервать;
		КонецЕсли;
		Для к = 1 По СтрДлина(ОбщийПуть) Цикл
			Если НЕ Сред(ОбщийПуть, к, 1) = Сред(МассивФайлов[й].ПолноеИмя, к, 1) Тогда
				ОбщийПуть = Лев(ОбщийПуть, к - 1);
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;

	Возврат ОбщийПуть;

КонецФункции

Функция ПовторСтроки(Строка, Количество) Экспорт
	Массив = Новый Массив();
	Для инд = 1 По Количество Цикл
		Массив.Добавить(Строка);
	КонецЦикла;
	Возврат СтрСоединить(Массив);
КонецФункции

Функция ФайлВТекст(ИмяФайла) Экспорт
	
	ЧтениеТекста = Новый ЧтениеТекста(ИмяФайла);
	Текст = ЧтениеТекста.Прочитать();
	ЧтениеТекста.Закрыть();
	
	Возврат Текст;
	
КонецФункции

Функция СовпаденияВТексте(Текст, Выражение) Экспорт
	
	РВ = Новый РегулярноеВыражение(Выражение);
	Совпадения = РВ.НайтиСовпадения(Текст);
	
	Возврат Совпадения;

КонецФункции

Функция СовпаденияВФайле(ИмяФайла, Выражение) Экспорт
	
	Текст = ФайлВТекст(ИмяФайла);

	Совпадения = СовпаденияВТексте(Текст, Выражение);
	
	Возврат Совпадения;

КонецФункции
